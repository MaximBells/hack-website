import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hackaton/blocks/auth/data/organisation_data.dart';
import 'package:hackaton/blocks/auth/data/role_data.dart';
import 'package:hackaton/blocks/auth/data/volunteer_data.dart';
import 'package:hackaton/blocks/auth/screens/login_screen.dart';
import 'package:hackaton/blocks/auth/screens/registration_screen.dart';
import 'package:hackaton/blocks/auth/screens/role_registration.dart';
import 'package:hackaton/blocks/catalogue/screens/catalogue_screen.dart';
import 'package:hackaton/blocks/loading/screens/loading_screen.dart';
import 'package:hackaton/static/app_color.dart';

import './routes.dart' as Routes;

import 'blocks/profile/screens/profile_screen.dart';

void main() {
  runApp(MaterialApp(
    initialRoute: '/',
    onGenerateRoute: (RouteSettings settings) {
      print(settings.name);
      switch (settings.name) {
        case Routes.profile:
          return CupertinoPageRoute(
              builder: (_) => ProfileScreen(), settings: settings);
        case Routes.addRole:
          return RoleData.roleRegistration;
        case Routes.addVolunteerForm:
          return VolunteerData.volunteerForm;
        case Routes.addOrganisationForm:
          return OrganisationData.organisationForm;
        case Routes.register:
          return CupertinoPageRoute(
              builder: (_) => const RegistrationScreen(), settings: settings);
        case Routes.login:
          return CupertinoPageRoute(
              builder: (_) => const LoginScreen(), settings: settings);
        case '/':
          return CupertinoPageRoute(
              builder: (_) => const LoadingScreen(), settings: settings);
        case Routes.catalog:
          return CupertinoPageRoute(
              builder: (_) => CatalogueScreen(), settings: settings);

      }
    },
    theme: ThemeData(
        colorScheme: ColorScheme.fromSwatch().copyWith(
            primary: const Color(0xff0044CC), secondary: AppColor.blue)),
    debugShowCheckedModeBanner: false,
  ));
}
