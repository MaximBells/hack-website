import 'package:flutter/material.dart';
import 'package:hackaton/static/app_enum.dart';
import 'package:hackaton/static/app_image.dart';
import 'package:hackaton/static/app_model.dart';
import 'package:hackaton/ui/buttons/buttons.dart';

import '../widgets/anket_infio.dart';
import '../widgets/user_stats.dart';

class ProfileInfo extends StatefulWidget {
  @override
  State<ProfileInfo> createState() => ProfileInfoState();
}

class ProfileInfoState extends State<ProfileInfo> {
  User user = User(
      id: 23,
      email: 'misterbobot@yandex.ru',
      name: 'Тимофей',
      secondName: 'акцак',
      password: 'ferfrefre');

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: 200, vertical: 150),
        decoration: BoxDecoration(color: Colors.white),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(
              flex: 2,
              child: Container(
                  decoration: BoxDecoration(),
                  child: CircleAvatar(
                    radius: 160, // Image radius
                    backgroundImage:
                        NetworkImage('https://picsum.photos/250?image=9'),
                  )),
            ),
            Expanded(
              child: SizedBox(),
              flex: 1,
            ),
            Expanded(
                flex: 6,
                child: Container(
                  padding: EdgeInsets.symmetric(vertical: 15),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        child: Text(
                          user.name,
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 24,
                              fontWeight: FontWeight.w600),
                        ),
                      ),
                      Container(
                          margin: EdgeInsets.only(top: 20),
                          child: UserStats(
                            coins: 123,
                            count: 123,
                            hours: 10,
                          )),
                      Container(
                        margin: EdgeInsets.only(top: 40),
                        child: AnketInfo(
                          form: VolunteerForm(
                              coins: 1,
                              description:
                                  'ewdewdwekfn erwjnfernfier ver fver fveirv ',
                              phone: '+79897949179',
                              hours: 3,
                              id: 3,
                              prefs: [PrefsType.animal, PrefsType.corporate]),
                        ),
                      ),
                      DefaultButton(
                          text: 'Организовать мероприятие', onClick: () => {})
                    ],
                  ),
                ))
          ],
        ),
      ),
    );
  }
}
