import 'package:flutter/material.dart';
import 'package:hackaton/blocks/profile/screens/profile_info.dart';

import '../../../ui/header.dart';

class ProfileScreen extends StatefulWidget {
  @override
  State<ProfileScreen> createState() => ProfileScreenState();
}

class ProfileScreenState extends State<ProfileScreen> {
  int activeTab = 0;

  List tabs = [ProfileInfo()];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          Header(),
          Expanded(
            child: Column(
              children: [Expanded(child: tabs[activeTab])],
            ),
          )
        ],
      ),
    );
  }
}
