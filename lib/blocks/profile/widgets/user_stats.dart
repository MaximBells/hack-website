import 'package:flutter/material.dart';

import '../../../static/app_image.dart';

Widget statBox(String title, String value, Color color, String icon) {
  return Row(
    children: [
      Container(
        height: 60,
        width: 60,
        decoration:
            BoxDecoration(color: color, borderRadius: BorderRadius.circular(8)),
        margin: EdgeInsets.only(right: 10),
        padding: EdgeInsets.all(12),
        child: Image.asset(
          icon,
          height: 30,
          width: 30,
        ),
      ),
      Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            value,
            style: TextStyle(
                color: Colors.black, fontSize: 18, fontWeight: FontWeight.w700),
          ),
          Text(
            title,
            style: TextStyle(
                color: Color(0xff888888),
                fontSize: 10,
                fontWeight: FontWeight.w400),
          )
        ],
      )
    ],
  );
}

Widget ProgressBar(int hours) {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.end,
    children: [
      Text(
        'опытный бобер',
        style: TextStyle(
        color: Color(0xff888888),
        fontSize: 10,
        fontWeight: FontWeight.w500),
      ),
      SizedBox(height: 5,),
      ClipRRect(
        borderRadius: BorderRadius.circular(10),
        child: LinearProgressIndicator(
        value: hours / 100,
        color: Color.fromARGB(107, 196, 200, 208),
        valueColor:
            new AlwaysStoppedAnimation<Color>(Color.fromARGB(109, 0, 68, 204)),
        minHeight: 15,
      ),
    )
    ],
  );
}

class UserStats extends StatelessWidget {
  int hours;
  int coins;
  int count;

  UserStats({required this.coins, required this.count, required this.hours});

  @override
  Widget build(BuildContext context) {
    return Container(
        child: Column(children: [
          Container(
            margin: EdgeInsets.only(bottom: 40),
            child: ProgressBar(hours),
          ),
      Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          statBox("Отработано часов", hours.toString(),
              Color.fromARGB(110, 157, 255, 60), AppImage.time),
          statBox("Мероприятий", count.toString(),
              Color.fromARGB(102, 255, 107, 60), AppImage.hand),
          statBox("Боброкоинов", coins.toString(),
              Color.fromARGB(102, 255, 212, 60), AppImage.bobrocoin),
        ],
      ),
    ]));
  }
}
