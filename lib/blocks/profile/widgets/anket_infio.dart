import 'package:flutter/material.dart';
import 'package:hackaton/blocks/auth/widgets/pref_container.dart';

import '../../../static/app_enum.dart';
import '../../../static/app_model.dart';

Widget anketData(String title, String value, double margin) {
  return Container(
    margin: EdgeInsets.only(bottom: margin),
    child: Row(
      children: [
        Text(
          title+":",
          style: TextStyle(
              color: Colors.black, fontWeight: FontWeight.w600, fontSize: 12),
        ),
        SizedBox(
          width: 10,
        ),
        Text(
          value,
          style: TextStyle(
              color: Colors.black, fontWeight: FontWeight.w300, fontSize: 10),
        )
      ],
    ),
  );
}

class AnketInfo extends StatelessWidget {
  VolunteerForm form;
  AnketInfo({required this.form});
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        anketData('Телефон', form.phone, 25),
        anketData('Возраст', "18", 15),
        Container(
          margin: EdgeInsets.only(bottom: 15),
          child: Row(
            children: [
              Text(
                'Навыки:',
                style: TextStyle(
                    color: Colors.black, fontWeight: FontWeight.w600, fontSize: 12),
              ),
              SizedBox(
                width: 10,
              ),
              Row(
                children: form.prefs.map((e) => PrefContainer(
                                text: prefsMap[e]!,
                                type: e,
                              ))
                .toList(),
              )
            ],
          ),
        ),
        anketData('О себе', form.description, 25),
      ],
    );
  }
}
