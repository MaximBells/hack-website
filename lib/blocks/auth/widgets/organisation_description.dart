import 'package:flutter/material.dart';

import '../../../static/app.dart';
import '../../../ui/forms/input-form.dart';
import '../data/organisation_controller.dart';
import 'adaptive_text.dart';

class OrganisationDescription extends StatelessWidget {
  const OrganisationDescription({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(top: 10),
      width: double.infinity,
      child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              margin: const EdgeInsets.only(bottom: 10),
              child: AdaptiveText(
                text: 'Описание деятельности',
                fontSize: 14,
                fontWeight: FontWeight.w600,
              ),
            ),
            InputForm(
              mustWrite: false,
              onChange: (String value) {
                getIt<OrganisationController>().setDescription(value: value);
              },
              placeholder: '',
              minLines: 10,
              maxLines: null,
            ),
          ]),
    );
  }
}
