import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:hackaton/blocks/auth/data/volunteer_controller.dart';
import 'package:hackaton/blocks/auth/widgets/pref_container.dart';
import 'package:hackaton/static/app.dart';
import 'package:hackaton/static/app_color.dart';
import 'package:hackaton/static/app_enum.dart';

import '../../../ui/forms/input-form.dart';
import 'adaptive_text.dart';

class PrefsVolunteer extends StatelessWidget {
  const PrefsVolunteer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(top: 10),
      width: double.infinity,
      child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              margin: const EdgeInsets.only(bottom: 10),
              child: AdaptiveText(
                text: 'Направления волонтерства',
                fontSize: 14,
                fontWeight: FontWeight.w600,
              ),
            ),
            Container(
              padding: const EdgeInsets.only(
                  top: 12, left: 24, right: 24, bottom: 36),
              width: 1200,
              constraints: const BoxConstraints(maxWidth: 1200, minWidth: 100),
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(4),
                  border: Border.all(color: AppColor.lightGrey, width: 2)),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [],
                  ),
                  Observer(builder: (context) {
                    return Wrap(
                      alignment: WrapAlignment.start,
                      children: getIt<VolunteerController>()
                          .userPrefs
                          .map((e) => PrefContainer(
                                text: prefsMap[e]!,
                                type: e,
                              ))
                          .toList(),
                    );
                  })
                ],
              ),
            ),
            Container(
              padding: const EdgeInsets.only(
                  top: 12, left: 24, right: 24, bottom: 48),
              constraints: const BoxConstraints(maxWidth: 1200, minWidth: 100),
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(4),
                  border: Border.all(color: AppColor.lightGrey, width: 2)),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      AdaptiveText(
                        text: 'Виды волонтерства',
                        fontWeight: FontWeight.w600,
                        fontSize: 10,
                      ),
                    ],
                  ),
                  Observer(builder: (context) {
                    return Wrap(
                      alignment: WrapAlignment.start,
                      children: getIt<VolunteerController>()
                          .initPrefs
                          .map((e) => PrefContainer(
                                text: prefsMap[e]!,
                                type: e,
                              ))
                          .toList(),
                    );
                  })
                ],
              ),
            ),
          ]),
    );
  }
}
