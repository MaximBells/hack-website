import 'package:flutter/material.dart';
import 'package:hackaton/blocks/auth/data/role_controller.dart';
import 'package:hackaton/static/app.dart';

import '../../../static/app_enum.dart';
import '../../../static/app_string.dart';
import '../../../static/app_style.dart';
import '../../../static/sizehelpers.dart';
import 'adaptive_text.dart';
import 'choose_role.dart';
import 'package:hackaton/routes.dart' as Routes;

class RoleRegistrationBody extends StatelessWidget {
  const RoleRegistrationBody({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    bool mobile = isMobile(context);
    if (mobile) {
      return SizedBox(
        width: double.infinity,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              height: 400,
              child: ListView(
                scrollDirection: Axis.horizontal,
                children: const [
                  ChooseRole(
                    type: UserType.Volunteer,
                  ),
                  ChooseRole(type: UserType.Organisation),
                ],
              ),
            ),
            Container(
              width: double.infinity,
              margin: const EdgeInsets.only(bottom: 15, top: 40),
              padding: EdgeInsets.symmetric(horizontal: 15),
              child: ElevatedButton(
                onPressed: () {
                  Navigator.pushReplacementNamed(context, Routes.profile);
                },
                child: const Text(
                  AppString.skip,
                  style: TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.w500,
                      fontFamily: 'Golos'),
                ),
                style: AppStyle.outLineStyle(context),
              ),
            ),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 15),
              width: double.infinity,
              child: ElevatedButton(
                onPressed: () {
                  getIt<RoleController>().goNextStep();
                },
                child: const Text(
                  AppString.next,
                  style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.w500,
                      fontFamily: 'Golos'),
                ),
                style: AppStyle.standardStyle(context),
              ),
            ),
          ],
        ),
      );
    }

    return SizedBox(
      width: double.infinity,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          AdaptiveText(
            text: AppString.registration,
            fontSize: 30,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: const [
              ChooseRole(
                type: UserType.Volunteer,
              ),
              ChooseRole(type: UserType.Organisation),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                margin: const EdgeInsets.only(right: 12, bottom: 100),
                child: ElevatedButton(
                  onPressed: () {
                    Navigator.pushReplacementNamed(context, Routes.profile);
                  },
                  child: const Text(
                    AppString.skip,
                    style: TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.w500,
                        fontFamily: 'Golos'),
                  ),
                  style: AppStyle.outLineStyle(context),
                ),
              ),
              Container(
                margin: const EdgeInsets.only(left: 12, bottom: 100),
                child: ElevatedButton(
                  onPressed: () {
                    getIt<RoleController>().goNextStep();
                  },
                  child: const Text(
                    AppString.next,
                    style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.w500,
                        fontFamily: 'Golos'),
                  ),
                  style: AppStyle.standardStyle(context),
                ),
              ),
            ],
          )
        ],
      ),
    );
  }
}
