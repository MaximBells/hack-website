import 'package:flutter/material.dart';

import '../../../static/app.dart';
import '../../../ui/forms/input-form.dart';
import '../data/organisation_controller.dart';
import 'adaptive_text.dart';

class OrganisationEmail extends StatelessWidget {
  const OrganisationEmail({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(top: 30),
      width: double.infinity,
      child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              margin: const EdgeInsets.only(bottom: 10),
              child: AdaptiveText(
                text: 'Почта*',
                fontSize: 14,
                fontWeight: FontWeight.w600,
              ),
            ),
            InputForm(
              onChange: (String value) {
                getIt<OrganisationController>().setEmail(value: value);
              },
              placeholder: 'ivanov@mail.ru',
            ),
          ]),
    );
  }
}
