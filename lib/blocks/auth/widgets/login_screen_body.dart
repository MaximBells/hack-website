import 'package:animate_do/animate_do.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:hackaton/blocks/auth/screens/login_screen.dart';

import '../../../static/app_color.dart';
import '../../../static/app_image.dart';
import '../../../static/app_string.dart';
import '../../../static/sizehelpers.dart';
import '../../../ui/social-networks.dart';
import 'adaptive_text.dart';

class LoginScreenBody extends StatelessWidget {
  const LoginScreenBody({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    bool mobile = isMobile(context);
    double width = getWidth(context);

    if (mobile) {
      return Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Expanded(
            flex: 6,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                const AutoSizeText(
                  'Вход',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontFamily: 'Golos',
                      fontSize: 32,
                      fontWeight: FontWeight.bold,
                      color: Colors.black),
                  maxLines: 1,
                ),
                SizedBox(
                  width: width * 0.7,
                  child: SocialNetworks(),
                ),
                const LoginForm(),
                const SizedBox()
              ],
            ),
          ),
          Expanded(
              flex: width < 400 ? 2 : 4,
              child: Container(
                height: double.infinity,
                child: Bounce(
                    duration: const Duration(milliseconds: 2500),
                    delay: const Duration(seconds: 1),
                    from: 20,
                    infinite: true,
                    child: Image.asset(
                      AppImage.logo,
                      fit: BoxFit.contain,
                    )),
              )),
        ],
      );
    }
    return Row(
      children: [
        Expanded(
            child: Container(
          height: double.infinity,
          decoration: const BoxDecoration(
            color: AppColor.brown,
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              AdaptiveText(
                text: AppString.youGetCaught,
                color: Colors.white,
              ),
              Bounce(
                  duration: const Duration(milliseconds: 2500),
                  delay: const Duration(seconds: 1),
                  from: 20,
                  infinite: true,
                  child: Image.asset(AppImage.logo)),
              AdaptiveText(
                text: AppString.inviteBobrite,
                color: Colors.white,
                fontSize: 20,
                maxLines: 2,
              )
            ],
          ),
        )),
        Expanded(
            child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const AutoSizeText(
              'Вход',
              textAlign: TextAlign.center,
              style: TextStyle(
                  fontFamily: 'Golos',
                  fontSize: 30,
                  fontWeight: FontWeight.w600,
                  color: Colors.black),
              maxLines: 1,
            ),
            const SizedBox(
              height: 30,
            ),
            SizedBox(
              width: 400,
              child: SocialNetworks(),
            ),
            const SizedBox(
              height: 40,
            ),
            const LoginForm(),
            const SizedBox()
          ],
        )),
      ],
    );
  }
}
