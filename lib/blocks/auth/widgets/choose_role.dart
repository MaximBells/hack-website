import 'dart:math';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:hackaton/blocks/auth/data/role_controller.dart';
import 'package:hackaton/blocks/auth/widgets/adaptive_text.dart';
import 'package:hackaton/static/app.dart';
import 'package:hackaton/static/app_color.dart';
import 'package:hackaton/static/app_enum.dart';
import 'package:hackaton/static/app_image.dart';
import 'package:hackaton/static/app_string.dart';

import '../../../static/sizehelpers.dart';

class ChooseRole extends StatelessWidget {
  const ChooseRole({Key? key, required this.type}) : super(key: key);
  final UserType type;

  @override
  Widget build(BuildContext context) {
    double width = getWidth(context);
    return GestureDetector(
      onTap: () {
        getIt<RoleController>().changeUserType(type);
      },
      child: Observer(builder: (context) {
        return AnimatedContainer(
          duration: const Duration(milliseconds: 250),
          margin: const EdgeInsets.all(12),
          padding:
              const EdgeInsets.only(right: 15, left: 15, top: 30, bottom: 30),
          height: 350,
          width: min(325, width*0.8),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(12),
              color: Colors.white,
              border: Border.all(
                  width: 2,
                  color: getIt<RoleController>().userType == type
                      ? Theme.of(context).colorScheme.secondary
                      : AppColor.lightGrey)),
          child: Stack(
            alignment: Alignment.topRight,
            children: [
              AnimatedContainer(
                duration: const Duration(milliseconds: 250),
                width: 20,
                height: 20,
                margin: const EdgeInsets.only(right: 10, bottom: 5),
                alignment: Alignment.topRight,
                decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    border: Border.all(
                        width: getIt<RoleController>().userType == type ? 6 : 3,
                        color: getIt<RoleController>().userType == type
                            ? Theme.of(context).colorScheme.secondary
                            : AppColor.lightGrey)),
              ),
              Align(
                alignment: Alignment.center,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Image.asset(type == UserType.Volunteer
                        ? AppImage.volunteer
                        : AppImage.organisation),
                    AdaptiveText(
                      text: type == UserType.Volunteer
                          ? AppString.iAmVolunteer
                          : AppString.organisation,
                      maxLines: 1,
                      fontSize: 16,
                      fontWeight: FontWeight.bold,
                    ),
                    AdaptiveText(
                      text: type == UserType.Volunteer
                          ? AppString.iDoNotHaveOrganisation
                          : AppString.iRepresentOrganisation,
                      fontSize: 8,
                      fontWeight: FontWeight.normal,
                      maxLines: 4,
                    )
                  ],
                ),
              ),
            ],
          ),
        );
      }),
    );
  }
}
