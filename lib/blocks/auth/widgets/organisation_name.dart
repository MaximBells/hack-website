import 'package:flutter/material.dart';
import 'package:hackaton/blocks/auth/data/organisation_controller.dart';
import 'package:hackaton/static/app.dart';

import '../../../ui/forms/input-form.dart';
import 'adaptive_text.dart';

class OrganisationName extends StatelessWidget {
  const OrganisationName({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(top: 72),
      width: double.infinity,
      child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              margin: const EdgeInsets.only(bottom: 10),
              child: AdaptiveText(
                text: 'Название организации*',
                fontSize: 14,
                fontWeight: FontWeight.w600,
              ),
            ),
            InputForm(
              onChange: (String value) {
                getIt<OrganisationController>()
                    .setOrganisationName(value: value);
              },
              placeholder: 'ООО Иванов',
            ),
          ]),
    );
  }
}
