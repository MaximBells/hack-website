import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';

import '../../../static/app_string.dart';

class AdaptiveText extends StatelessWidget {
  AdaptiveText(
      {Key? key,
      required this.text,
      this.color = Colors.black,
      this.fontWeight = FontWeight.bold,
      this.textDecoration,
      this.fontSize = 24,
      this.maxLines = 1})
      : super(key: key);
  final String text;
  Color color = Colors.black;
  double fontSize = 24;
  FontWeight fontWeight = FontWeight.bold;
  int maxLines = 1;
  TextDecoration? textDecoration;

  @override
  Widget build(BuildContext context) {
    return AutoSizeText(
      text,
      textAlign: TextAlign.center,
      style: TextStyle(
          fontFamily: 'Golos',
          fontSize: fontSize,
          fontWeight: fontWeight,
          decoration: textDecoration,
          color: color),
      maxLines: maxLines,
    );
  }
}
