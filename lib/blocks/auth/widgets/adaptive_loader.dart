import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:hackaton/static/app_color.dart';

class AdaptiveLoader extends StatelessWidget {
  AdaptiveLoader({Key? key, required this.isLoading, required this.child})
      : super(key: key);
  bool isLoading;
  final Widget child;

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.center,
      children: [
        AnimatedOpacity(
          opacity: isLoading == true ? 0 : 1,
          duration: const Duration(milliseconds: 500),
          child: child,
        ),
        AnimatedOpacity(
          opacity: isLoading == true ? 1 : 0,
          duration: const Duration(milliseconds: 500),
          child: Pulse(
            infinite: true,
            delay: const Duration(milliseconds: 250),
            duration: const Duration(milliseconds: 750),
            child: Container(
              decoration: const BoxDecoration(
                  color: AppColor.blue, shape: BoxShape.circle),
              padding: const EdgeInsets.all(6),
              child: const CircularProgressIndicator(
                color: Colors.white,
              ),
            ),
          ),
        ),
      ],
    );
  }
}
