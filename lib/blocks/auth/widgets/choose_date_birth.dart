import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:hackaton/blocks/auth/data/volunteer_controller.dart';
import 'package:hackaton/static/app.dart';
import 'package:hackaton/static/app_color.dart';
import 'package:hackaton/static/app_extension.dart';
import 'package:hackaton/ui/forms/input-form.dart';

import 'adaptive_text.dart';

class ChooseDateBirth extends StatelessWidget {
  const ChooseDateBirth({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(top: 75),
      width: double.infinity,
      child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              margin: const EdgeInsets.only(bottom: 10),
              child: AdaptiveText(
                text: 'Дата рожденния*',
                fontSize: 14,
                fontWeight: FontWeight.w600,
              ),
            ),
            InputForm(
              onChange: (String value) {},
              controller: getIt<VolunteerController>().dateBirthController,
              placeholder: '01.01.2000',
              onPressed: () {
                ScaffoldMessenger.of(context).showSnackBar(
                  SnackBar(
                      elevation: 10,
                      backgroundColor: Colors.green,
                      behavior: SnackBarBehavior.floating,
                      dismissDirection: DismissDirection.up,
                      margin: EdgeInsets.only(
                          bottom: MediaQuery.of(context).size.height * 0.9),
                      duration: const Duration(seconds: 15),
                      content: AdaptiveText(
                        text: 'Для выбора даты крутите колесиком мышки',
                        maxLines: 2,
                        fontWeight: FontWeight.w500,
                        color: Colors.white,
                      )),
                );
                DatePicker.showDatePicker(
                        getIt<VolunteerController>().buildContext ?? context,
                        showTitleActions: true,
                        minTime: DateTime(1900, 3, 5),
                        maxTime: DateTime.now(), onCancel: () {
                  ScaffoldMessenger.of(context).hideCurrentSnackBar();
                }, onConfirm: (date) {
                  getIt<VolunteerController>().setDateBirth(dateTime: date);
                  ScaffoldMessenger.of(context).hideCurrentSnackBar();
                },
                        onChanged: (date) {},
                        currentTime: getIt<VolunteerController>().dateBirth,
                        locale: LocaleType.ru)
                    .then((value) =>
                        ScaffoldMessenger.of(context).hideCurrentSnackBar());
              },
            )
          ]),
    );
  }
}
