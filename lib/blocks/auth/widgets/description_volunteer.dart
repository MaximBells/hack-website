import 'package:flutter/material.dart';

import '../../../static/app.dart';
import '../../../ui/forms/input-form.dart';
import '../data/volunteer_controller.dart';
import 'adaptive_text.dart';

class DescriptionVolunteer extends StatelessWidget {
  const DescriptionVolunteer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(top: 10),
      width: double.infinity,
      child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              margin: const EdgeInsets.only(bottom: 10),
              child: AdaptiveText(
                text: 'О себе',
                fontSize: 14,
                fontWeight: FontWeight.w600,
              ),
            ),
            InputForm(
              mustWrite: false,
              onChange: (String value) {
                getIt<VolunteerController>().setDescription(value: value);
              },
              placeholder: '',
              minLines: 10,
              maxLines: null,
            ),
          ]),
    );
  }
}
