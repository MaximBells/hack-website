import 'package:flutter/material.dart';
import 'package:hackaton/blocks/auth/data/volunteer_controller.dart';
import 'package:hackaton/static/app.dart';
import 'package:hackaton/ui/formatters/phone_formatter.dart';
import 'package:hackaton/ui/forms/input-form.dart';

import 'adaptive_text.dart';

class ChoosePhoneVolunteer extends StatelessWidget {
  const ChoosePhoneVolunteer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(top: 30),
      width: double.infinity,
      child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              margin: const EdgeInsets.only(bottom: 10),
              child: AdaptiveText(
                text: 'Телефон*',
                fontSize: 14,
                fontWeight: FontWeight.w600,
              ),
            ),
            InputForm(
              onChange: (String value) {
                getIt<VolunteerController>().setPhone(value: value);
              },
              placeholder: '+7 (777) 777-77-77',
              textInputType: TextInputType.phone,
              textInputFormatter: PhoneFormatter(),
            ),
          ]),
    );
  }
}
