import 'package:flutter/material.dart';
import 'package:hackaton/blocks/auth/data/volunteer_controller.dart';
import 'package:hackaton/blocks/auth/widgets/adaptive_text.dart';
import 'package:hackaton/static/app.dart';
import 'package:hackaton/static/app_color.dart';
import 'package:hackaton/static/app_enum.dart';

class PrefContainer extends StatelessWidget {
  const PrefContainer({Key? key, required this.text, required this.type})
      : super(key: key);
  final String text;
  final PrefsType type;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(right: 6, top: 6, bottom: 6),
      child: ElevatedButton(
          style: ElevatedButton.styleFrom(
            primary: AppColor.lightBlue,
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(4)),
            padding:
                const EdgeInsets.only(left: 24, right: 24, bottom: 12, top: 12),
          ),
          onPressed: () => getIt<VolunteerController>().pressPrefs(pref: type),
          child: AdaptiveText(
            text: text,
            color: AppColor.blue,
            fontWeight: FontWeight.w600,
            fontSize: 10,
            maxLines: 1,
          )),
    );
  }
}
