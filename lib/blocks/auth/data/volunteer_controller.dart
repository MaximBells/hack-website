import 'package:flutter/cupertino.dart';
import 'package:hackaton/blocks/auth/screens/volunteer_form.dart';
import 'package:hackaton/static/app.dart';
import 'package:hackaton/static/app_enum.dart';
import 'package:hackaton/static/app_extension.dart';
import 'package:hackaton/static/app_model.dart' as form;
import 'package:hackaton/static/repository/users.dart';
import 'package:mobx/mobx.dart';
import 'package:hackaton/routes.dart' as Routes;

part 'volunteer_controller.g.dart';

class VolunteerController extends _VolunteerController
    with _$VolunteerController {}

abstract class _VolunteerController with Store {
  final TextEditingController dateBirthController = TextEditingController();
  final TextEditingController prefController = TextEditingController();

  @observable
  DateTime dateBirth = DateTime.now();

  @observable
  String phone = '';

  @observable
  String description = 'empty';

  @observable
  BuildContext? buildContext;

  @observable
  bool isLoading = false;

  @observable
  ObservableList<PrefsType> userPrefs = ObservableList.of([]);

  @observable
  ObservableList<PrefsType> initPrefs = ObservableList.of(PrefsType.values);

  @action
  void setContext(BuildContext value) => buildContext = value;

  @action
  void setDateBirth({required DateTime dateTime}) {
    dateBirth = dateTime;
    dateBirthController.text = dateBirth.fixedDate;
  }

  @action
  void setPhone({required String value}) => phone = value;

  @action
  void setDescription({required String value}) => description = value;

  @action
  void pressPrefs({required PrefsType pref}) {
    if (userPrefs.contains(pref)) {
      userPrefs.remove(pref);
      initPrefs.add(pref);
    } else if (initPrefs.contains(pref)) {
      initPrefs.remove(pref);
      userPrefs.add(pref);
    }
  }

  @action
  Future<void> nextStep() async {
    isLoading = true;
    var volunteer = form.VolunteerForm(
        id: App.controller.user!.id!,
        phone: phone,
        description: description,
        prefs: userPrefs,
        coins: 0,
        hours: 0);
    var createVolunteerResult = await createVolunteer(form: volunteer);
    print('here');
    bool updateTypeResult = false;
    if (createVolunteerResult) {
      updateTypeResult = await updateType(
          user: App.controller.user!, type: UserType.Volunteer);
    }
    if (updateTypeResult) {
      Navigator.pushReplacementNamed(buildContext!, Routes.profile);
    }
    isLoading = false;
  }
}
