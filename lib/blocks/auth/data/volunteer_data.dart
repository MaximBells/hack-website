import 'package:flutter/cupertino.dart';
import 'package:hackaton/blocks/auth/data/volunteer_controller.dart';
import 'package:hackaton/blocks/auth/screens/volunteer_form.dart';
import 'package:hackaton/static/app.dart';
import 'package:hackaton/routes.dart' as Routes;

class VolunteerData {
  static void register() {
    if (!getIt.isRegistered<VolunteerController>()) {
      getIt.registerLazySingleton<VolunteerController>(
          () => VolunteerController());
    }
  }

  static void unregister() {
    if (getIt.isRegistered<VolunteerController>()) {
      getIt.unregister<VolunteerController>();
    }
  }

  static CupertinoPageRoute get volunteerForm {
    register();
    return CupertinoPageRoute(
        builder: (context) => VolunteerForm(),
        settings: const RouteSettings(name: Routes.addVolunteerForm));
  }
}
