// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'volunteer_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic, no_leading_underscores_for_local_identifiers

mixin _$VolunteerController on _VolunteerController, Store {
  late final _$dateBirthAtom =
      Atom(name: '_VolunteerController.dateBirth', context: context);

  @override
  DateTime get dateBirth {
    _$dateBirthAtom.reportRead();
    return super.dateBirth;
  }

  @override
  set dateBirth(DateTime value) {
    _$dateBirthAtom.reportWrite(value, super.dateBirth, () {
      super.dateBirth = value;
    });
  }

  late final _$phoneAtom =
      Atom(name: '_VolunteerController.phone', context: context);

  @override
  String get phone {
    _$phoneAtom.reportRead();
    return super.phone;
  }

  @override
  set phone(String value) {
    _$phoneAtom.reportWrite(value, super.phone, () {
      super.phone = value;
    });
  }

  late final _$descriptionAtom =
      Atom(name: '_VolunteerController.description', context: context);

  @override
  String get description {
    _$descriptionAtom.reportRead();
    return super.description;
  }

  @override
  set description(String value) {
    _$descriptionAtom.reportWrite(value, super.description, () {
      super.description = value;
    });
  }

  late final _$buildContextAtom =
      Atom(name: '_VolunteerController.buildContext', context: context);

  @override
  BuildContext? get buildContext {
    _$buildContextAtom.reportRead();
    return super.buildContext;
  }

  @override
  set buildContext(BuildContext? value) {
    _$buildContextAtom.reportWrite(value, super.buildContext, () {
      super.buildContext = value;
    });
  }

  late final _$isLoadingAtom =
      Atom(name: '_VolunteerController.isLoading', context: context);

  @override
  bool get isLoading {
    _$isLoadingAtom.reportRead();
    return super.isLoading;
  }

  @override
  set isLoading(bool value) {
    _$isLoadingAtom.reportWrite(value, super.isLoading, () {
      super.isLoading = value;
    });
  }

  late final _$userPrefsAtom =
      Atom(name: '_VolunteerController.userPrefs', context: context);

  @override
  ObservableList<PrefsType> get userPrefs {
    _$userPrefsAtom.reportRead();
    return super.userPrefs;
  }

  @override
  set userPrefs(ObservableList<PrefsType> value) {
    _$userPrefsAtom.reportWrite(value, super.userPrefs, () {
      super.userPrefs = value;
    });
  }

  late final _$initPrefsAtom =
      Atom(name: '_VolunteerController.initPrefs', context: context);

  @override
  ObservableList<PrefsType> get initPrefs {
    _$initPrefsAtom.reportRead();
    return super.initPrefs;
  }

  @override
  set initPrefs(ObservableList<PrefsType> value) {
    _$initPrefsAtom.reportWrite(value, super.initPrefs, () {
      super.initPrefs = value;
    });
  }

  late final _$nextStepAsyncAction =
      AsyncAction('_VolunteerController.nextStep', context: context);

  @override
  Future<void> nextStep() {
    return _$nextStepAsyncAction.run(() => super.nextStep());
  }

  late final _$_VolunteerControllerActionController =
      ActionController(name: '_VolunteerController', context: context);

  @override
  void setContext(BuildContext value) {
    final _$actionInfo = _$_VolunteerControllerActionController.startAction(
        name: '_VolunteerController.setContext');
    try {
      return super.setContext(value);
    } finally {
      _$_VolunteerControllerActionController.endAction(_$actionInfo);
    }
  }

  @override
  void setDateBirth({required DateTime dateTime}) {
    final _$actionInfo = _$_VolunteerControllerActionController.startAction(
        name: '_VolunteerController.setDateBirth');
    try {
      return super.setDateBirth(dateTime: dateTime);
    } finally {
      _$_VolunteerControllerActionController.endAction(_$actionInfo);
    }
  }

  @override
  void setPhone({required String value}) {
    final _$actionInfo = _$_VolunteerControllerActionController.startAction(
        name: '_VolunteerController.setPhone');
    try {
      return super.setPhone(value: value);
    } finally {
      _$_VolunteerControllerActionController.endAction(_$actionInfo);
    }
  }

  @override
  void setDescription({required String value}) {
    final _$actionInfo = _$_VolunteerControllerActionController.startAction(
        name: '_VolunteerController.setDescription');
    try {
      return super.setDescription(value: value);
    } finally {
      _$_VolunteerControllerActionController.endAction(_$actionInfo);
    }
  }

  @override
  void pressPrefs({required PrefsType pref}) {
    final _$actionInfo = _$_VolunteerControllerActionController.startAction(
        name: '_VolunteerController.pressPrefs');
    try {
      return super.pressPrefs(pref: pref);
    } finally {
      _$_VolunteerControllerActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
dateBirth: ${dateBirth},
phone: ${phone},
description: ${description},
buildContext: ${buildContext},
isLoading: ${isLoading},
userPrefs: ${userPrefs},
initPrefs: ${initPrefs}
    ''';
  }
}
