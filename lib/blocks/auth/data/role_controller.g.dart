// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'role_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic, no_leading_underscores_for_local_identifiers

mixin _$RoleController on _RoleController, Store {
  late final _$userTypeAtom =
      Atom(name: '_RoleController.userType', context: context);

  @override
  UserType get userType {
    _$userTypeAtom.reportRead();
    return super.userType;
  }

  @override
  set userType(UserType value) {
    _$userTypeAtom.reportWrite(value, super.userType, () {
      super.userType = value;
    });
  }

  late final _$isVolunteerPressedAtom =
      Atom(name: '_RoleController.isVolunteerPressed', context: context);

  @override
  bool get isVolunteerPressed {
    _$isVolunteerPressedAtom.reportRead();
    return super.isVolunteerPressed;
  }

  @override
  set isVolunteerPressed(bool value) {
    _$isVolunteerPressedAtom.reportWrite(value, super.isVolunteerPressed, () {
      super.isVolunteerPressed = value;
    });
  }

  late final _$isOrganisationPressedAtom =
      Atom(name: '_RoleController.isOrganisationPressed', context: context);

  @override
  bool get isOrganisationPressed {
    _$isOrganisationPressedAtom.reportRead();
    return super.isOrganisationPressed;
  }

  @override
  set isOrganisationPressed(bool value) {
    _$isOrganisationPressedAtom.reportWrite(value, super.isOrganisationPressed,
        () {
      super.isOrganisationPressed = value;
    });
  }

  late final _$buildContextAtom =
      Atom(name: '_RoleController.buildContext', context: context);

  @override
  BuildContext? get buildContext {
    _$buildContextAtom.reportRead();
    return super.buildContext;
  }

  @override
  set buildContext(BuildContext? value) {
    _$buildContextAtom.reportWrite(value, super.buildContext, () {
      super.buildContext = value;
    });
  }

  late final _$_RoleControllerActionController =
      ActionController(name: '_RoleController', context: context);

  @override
  void setContext(BuildContext value) {
    final _$actionInfo = _$_RoleControllerActionController.startAction(
        name: '_RoleController.setContext');
    try {
      return super.setContext(value);
    } finally {
      _$_RoleControllerActionController.endAction(_$actionInfo);
    }
  }

  @override
  void pressRole(UserType type) {
    final _$actionInfo = _$_RoleControllerActionController.startAction(
        name: '_RoleController.pressRole');
    try {
      return super.pressRole(type);
    } finally {
      _$_RoleControllerActionController.endAction(_$actionInfo);
    }
  }

  @override
  void changeUserType(UserType type) {
    final _$actionInfo = _$_RoleControllerActionController.startAction(
        name: '_RoleController.changeUserType');
    try {
      return super.changeUserType(type);
    } finally {
      _$_RoleControllerActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
userType: ${userType},
isVolunteerPressed: ${isVolunteerPressed},
isOrganisationPressed: ${isOrganisationPressed},
buildContext: ${buildContext}
    ''';
  }
}
