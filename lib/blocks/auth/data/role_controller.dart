import 'package:flutter/cupertino.dart';
import 'package:hackaton/static/app_enum.dart';
import 'package:mobx/mobx.dart';
import 'package:hackaton/routes.dart' as Routes;

part 'role_controller.g.dart';

class RoleController extends _RoleController with _$RoleController {}

abstract class _RoleController with Store {
  @observable
  UserType userType = UserType.Common;

  @observable
  bool isVolunteerPressed = false;

  @observable
  bool isOrganisationPressed = false;

  @observable
  BuildContext? buildContext;

  @action
  void setContext(BuildContext value) => buildContext = value;

  @action
  void pressRole(UserType type) {
    if (type == UserType.Volunteer) {
      changeUserType(type);
    } else if (type == UserType.Organisation) {
      changeUserType(type);
    }
  }

  @action
  void changeUserType(UserType type) => userType = type;

  void goNextStep() {
    if (userType == UserType.Volunteer) {
      Navigator.pushReplacementNamed(buildContext!, Routes.addVolunteerForm);
    } else if (userType == UserType.Organisation) {
      Navigator.pushReplacementNamed(buildContext!, Routes.addOrganisationForm);
    }
  }
}
