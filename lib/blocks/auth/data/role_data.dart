import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hackaton/blocks/auth/data/role_controller.dart';
import 'package:hackaton/blocks/auth/screens/role_registration.dart';
import 'package:hackaton/static/app.dart';
import 'package:hackaton/routes.dart' as Routes;

import '../../../static/app_enum.dart';

class RoleData {
  static void register() {
    if (!getIt.isRegistered<RoleController>()) {
      getIt.registerLazySingleton(() => RoleController());
    }
  }

  static void unregister() {
    if (getIt.isRegistered<RoleController>()) {
      getIt.unregister<RoleController>();
    }
  }

  static CupertinoPageRoute get roleRegistration {
    register();
    return CupertinoPageRoute(builder: (context) => const RoleRegistration(), settings: RouteSettings(name: Routes.addRole));
  }
}
