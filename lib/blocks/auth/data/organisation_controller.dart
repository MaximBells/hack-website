import 'package:flutter/cupertino.dart';
import 'package:hackaton/static/app.dart';
import 'package:hackaton/static/app_enum.dart';
import 'package:hackaton/static/repository/users.dart';
import 'package:mobx/mobx.dart';
import 'package:hackaton/static/app_model.dart' as model;
import 'package:hackaton/routes.dart' as Routes;

part 'organisation_controller.g.dart';

class OrganisationController extends _OrganisationController
    with _$OrganisationController {}

abstract class _OrganisationController with Store {
  @observable
  BuildContext? buildContext;

  @observable
  String organisationName = "";

  @observable
  String email = "";

  @observable
  String site = "";

  @observable
  String description = "";

  @observable
  bool isLoading = false;

  @action
  void setOrganisationName({required String value}) => organisationName = value;

  @action
  void setEmail({required String value}) => email = value;

  @action
  void setSite({required String value}) => site = value;

  @action
  void setDescription({required String value}) => description = value;

  @action
  void setContext(BuildContext value) => buildContext = value;

  @action
  Future<void> goNextStep() async {
    isLoading = true;
    var form = model.Organisation(
      id: 0,
      description: description,
      emailOrganisation: email,
      nameOrganisation: organisationName,
      site: site,
    );
    var createOrganisationResult = await createOrganisation(form: form);
    bool updateTypeResult = false;
    if (createOrganisationResult) {
      updateTypeResult = await updateType(
          user: App.controller.user!, type: UserType.Organisation);
    }
    if (updateTypeResult) {
      Navigator.pushReplacementNamed(buildContext!, Routes.profile);
    }
    isLoading = false;
  }
}
