import 'package:flutter/cupertino.dart';
import 'package:hackaton/blocks/auth/data/organisation_controller.dart';
import 'package:hackaton/blocks/auth/screens/organisation_form.dart';
import 'package:hackaton/static/app.dart';
import 'package:hackaton/routes.dart' as Routes;

class OrganisationData {
  static void register() {
    if (!getIt.isRegistered<OrganisationController>()) {
      getIt.registerLazySingleton<OrganisationController>(
          () => OrganisationController());
    }
  }

  static void unregister() {
    if (getIt.isRegistered<OrganisationController>()) {
      getIt.unregister<OrganisationController>();
    }
  }

  static CupertinoPageRoute get organisationForm {
    register();
    return CupertinoPageRoute(
        builder: (context) => OrganisationForm(),
        settings: const RouteSettings(name: Routes.addOrganisationForm));
  }
}
