// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'organisation_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic, no_leading_underscores_for_local_identifiers

mixin _$OrganisationController on _OrganisationController, Store {
  late final _$buildContextAtom =
      Atom(name: '_OrganisationController.buildContext', context: context);

  @override
  BuildContext? get buildContext {
    _$buildContextAtom.reportRead();
    return super.buildContext;
  }

  @override
  set buildContext(BuildContext? value) {
    _$buildContextAtom.reportWrite(value, super.buildContext, () {
      super.buildContext = value;
    });
  }

  late final _$organisationNameAtom =
      Atom(name: '_OrganisationController.organisationName', context: context);

  @override
  String get organisationName {
    _$organisationNameAtom.reportRead();
    return super.organisationName;
  }

  @override
  set organisationName(String value) {
    _$organisationNameAtom.reportWrite(value, super.organisationName, () {
      super.organisationName = value;
    });
  }

  late final _$emailAtom =
      Atom(name: '_OrganisationController.email', context: context);

  @override
  String get email {
    _$emailAtom.reportRead();
    return super.email;
  }

  @override
  set email(String value) {
    _$emailAtom.reportWrite(value, super.email, () {
      super.email = value;
    });
  }

  late final _$siteAtom =
      Atom(name: '_OrganisationController.site', context: context);

  @override
  String get site {
    _$siteAtom.reportRead();
    return super.site;
  }

  @override
  set site(String value) {
    _$siteAtom.reportWrite(value, super.site, () {
      super.site = value;
    });
  }

  late final _$descriptionAtom =
      Atom(name: '_OrganisationController.description', context: context);

  @override
  String get description {
    _$descriptionAtom.reportRead();
    return super.description;
  }

  @override
  set description(String value) {
    _$descriptionAtom.reportWrite(value, super.description, () {
      super.description = value;
    });
  }

  late final _$isLoadingAtom =
      Atom(name: '_OrganisationController.isLoading', context: context);

  @override
  bool get isLoading {
    _$isLoadingAtom.reportRead();
    return super.isLoading;
  }

  @override
  set isLoading(bool value) {
    _$isLoadingAtom.reportWrite(value, super.isLoading, () {
      super.isLoading = value;
    });
  }

  late final _$goNextStepAsyncAction =
      AsyncAction('_OrganisationController.goNextStep', context: context);

  @override
  Future<void> goNextStep() {
    return _$goNextStepAsyncAction.run(() => super.goNextStep());
  }

  late final _$_OrganisationControllerActionController =
      ActionController(name: '_OrganisationController', context: context);

  @override
  void setOrganisationName({required String value}) {
    final _$actionInfo = _$_OrganisationControllerActionController.startAction(
        name: '_OrganisationController.setOrganisationName');
    try {
      return super.setOrganisationName(value: value);
    } finally {
      _$_OrganisationControllerActionController.endAction(_$actionInfo);
    }
  }

  @override
  void setEmail({required String value}) {
    final _$actionInfo = _$_OrganisationControllerActionController.startAction(
        name: '_OrganisationController.setEmail');
    try {
      return super.setEmail(value: value);
    } finally {
      _$_OrganisationControllerActionController.endAction(_$actionInfo);
    }
  }

  @override
  void setSite({required String value}) {
    final _$actionInfo = _$_OrganisationControllerActionController.startAction(
        name: '_OrganisationController.setSite');
    try {
      return super.setSite(value: value);
    } finally {
      _$_OrganisationControllerActionController.endAction(_$actionInfo);
    }
  }

  @override
  void setDescription({required String value}) {
    final _$actionInfo = _$_OrganisationControllerActionController.startAction(
        name: '_OrganisationController.setDescription');
    try {
      return super.setDescription(value: value);
    } finally {
      _$_OrganisationControllerActionController.endAction(_$actionInfo);
    }
  }

  @override
  void setContext(BuildContext value) {
    final _$actionInfo = _$_OrganisationControllerActionController.startAction(
        name: '_OrganisationController.setContext');
    try {
      return super.setContext(value);
    } finally {
      _$_OrganisationControllerActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
buildContext: ${buildContext},
organisationName: ${organisationName},
email: ${email},
site: ${site},
description: ${description},
isLoading: ${isLoading}
    ''';
  }
}
