import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:hackaton/blocks/auth/data/volunteer_controller.dart';
import 'package:hackaton/blocks/auth/widgets/adaptive_loader.dart';
import 'package:hackaton/blocks/auth/widgets/adaptive_text.dart';
import 'package:hackaton/blocks/auth/widgets/choose_date_birth.dart';
import 'package:hackaton/blocks/auth/widgets/choose_phone_volunteer.dart';
import 'package:hackaton/blocks/auth/widgets/description_volunteer.dart';
import 'package:hackaton/blocks/auth/widgets/prefs_volunteer.dart';
import 'package:hackaton/static/app.dart';
import 'package:hackaton/static/app_color.dart';
import 'package:hackaton/static/app_style.dart';
import 'package:hackaton/routes.dart' as Routes;

class VolunteerForm extends StatelessWidget {
  VolunteerForm({Key? key}) : super(key: key);
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    getIt<VolunteerController>().setContext(context);
    return Form(
      key: _formKey,
      child: Scaffold(
        backgroundColor: AppColor.grey,
        body: SizedBox(
          width: double.infinity,
          child: Center(
            child: SingleChildScrollView(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    margin: const EdgeInsets.only(top: 48, bottom: 48),
                    width: MediaQuery.of(context).size.width * 0.7,
                    padding: const EdgeInsets.only(
                        left: 100, right: 100, top: 50, bottom: 50),
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(12),
                        border: Border.all(color: AppColor.lightGrey)),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        AdaptiveText(text: 'Анкета волонтера'),
                        const ChooseDateBirth(),
                        const ChoosePhoneVolunteer(),
                        const DescriptionVolunteer(),
                        const PrefsVolunteer(),
                        Wrap(
                          alignment: WrapAlignment.center,
                          children: [
                            Container(
                              margin: const EdgeInsets.only(top: 60, right: 36),
                              child: ElevatedButton(
                                onPressed: () {
                                  Navigator.pushReplacementNamed(
                                      context, Routes.profile);
                                },
                                child: AdaptiveText(
                                  text: 'Пропустить',
                                  fontSize: 16,
                                  fontWeight: FontWeight.w500,
                                  color: Colors.black,
                                ),
                                style: AppStyle.outLineStyle(context,
                                    backgroundColor: Colors.white),
                              ),
                            ),
                            Container(
                              margin: const EdgeInsets.only(top: 60, left: 36),
                              child: Observer(builder: (context) {
                                return AdaptiveLoader(
                                    isLoading:
                                        getIt<VolunteerController>().isLoading,
                                    child: ElevatedButton(
                                      onPressed: () {
                                        if (_formKey.currentState!.validate() ==
                                            true) {
                                          getIt<VolunteerController>()
                                              .nextStep();
                                        }
                                      },
                                      child: AdaptiveText(
                                          text: 'Сохранить',
                                          fontSize: 16,
                                          fontWeight: FontWeight.w500,
                                          color: Colors.white),
                                      style: AppStyle.standardStyle(context),
                                    ));
                              }),
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
