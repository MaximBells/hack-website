import 'package:flutter/material.dart';
import 'package:hackaton/blocks/auth/data/role_controller.dart';
import 'package:hackaton/blocks/auth/widgets/adaptive_text.dart';
import 'package:hackaton/blocks/auth/widgets/choose_role.dart';
import 'package:hackaton/static/app.dart';
import 'package:hackaton/static/app_color.dart';
import 'package:hackaton/static/app_style.dart';
import 'package:hackaton/static/sizehelpers.dart';

import '../../../static/app_enum.dart';
import '../../../static/app_string.dart';
import '../widgets/role_registration_body.dart';

class RoleRegistration extends StatelessWidget {
  const RoleRegistration({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    getIt<RoleController>().setContext(context);
    return const Scaffold(
        backgroundColor: AppColor.grey, body: RoleRegistrationBody());
  }
}
