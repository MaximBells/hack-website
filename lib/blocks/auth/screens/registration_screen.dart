import 'dart:js';

import 'package:animate_do/animate_do.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:hackaton/blocks/auth/data/role_data.dart';
import 'package:hackaton/blocks/auth/widgets/adaptive_loader.dart';
import 'package:hackaton/blocks/auth/widgets/adaptive_text.dart';
import 'package:hackaton/blocks/auth/widgets/registration_screen_body.dart';
import 'package:hackaton/static/app.dart';
import 'package:hackaton/static/app_image.dart';
import 'package:hackaton/static/app_model.dart';
import 'package:hackaton/static/app_string.dart';
import 'package:hackaton/routes.dart' as Routes;

import '../../../static/app_color.dart';
import '../../../static/app_enum.dart';
import '../../../static/repository/users.dart';
import '../../../static/sizehelpers.dart';
import '../../../ui/buttons/buttons.dart';
import '../../../ui/forms/input-form.dart';
import '../../../ui/social-networks.dart';

class RegistrationForm extends StatefulWidget {
  @override
  State<RegistrationForm> createState() => RegistrationFormState();
}

class RegistrationFormState extends State<RegistrationForm> {
  String userEmail = '';
  String userName = '';
  String userPassword = '';
  bool isLoading = false;

  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    final mobile = isMobile(context);
    double offset = 100;
    if (mobile) {
      offset = 20;
    }
    return Container(
      margin: EdgeInsets.only(left: offset, right: offset),
      child: Form(
        key: _formKey,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const AutoSizeText(
                  "Имя*",
                  textAlign: TextAlign.start,
                  style: TextStyle(
                      fontFamily: 'Golos',
                      fontSize: 12,
                      fontWeight: FontWeight.bold,
                      color: Colors.black),
                  maxLines: 1,
                ),
                const SizedBox(
                  height: 10,
                ),
                InputForm(
                  backgroundColor: AppColor.grey,
                    placeholder: 'Иван Иванов',
                    onChange: (value) => setState(() {
                          userName = value;
                        })),
                const SizedBox(
                  height: 20,
                ),
                const AutoSizeText(
                  "Email*",
                  textAlign: TextAlign.left,
                  style: TextStyle(
                      fontFamily: 'Golos',
                      fontSize: 12,
                      fontWeight: FontWeight.bold,
                      color: Colors.black),
                  maxLines: 1,
                ),
                const SizedBox(
                  height: 10,
                ),
                InputForm(
                    backgroundColor: AppColor.grey,
                    placeholder: 'ivanova@mail.ru',
                    onChange: (value) => setState(() {
                          userEmail = value;
                        })),
                const SizedBox(
                  height: 20,
                ),
                const AutoSizeText(
                  "Пароль*",
                  textAlign: TextAlign.left,
                  style: TextStyle(
                      fontFamily: 'Golos',
                      fontSize: 12,
                      fontWeight: FontWeight.bold,
                      color: Colors.black),
                  maxLines: 1,
                ),
                const SizedBox(
                  height: 10,
                ),
                InputForm(
                    backgroundColor: AppColor.grey,
                    placeholder: 'Придумайте пароль',
                    onChange: (value) => setState(() {
                          userPassword = value;
                        })),
                const SizedBox(
                  height: 50,
                ),
              ],
            ),
            Container(
              margin: const EdgeInsets.only(bottom: 12),
              child: Center(
                child: AdaptiveLoader(
                  isLoading: isLoading,
                  child: DefaultButton(
                    text: 'Зарегистироваться',
                    onClick: () {
                      if (_formKey.currentState!.validate() &&
                          isLoading == false) {
                        setState(() {
                          isLoading = true;
                        });
                        registerUser(
                                user: User(
                                    id: null,
                                    email: userEmail,
                                    name: userName,
                                    secondName: '',
                                    password: userPassword))
                            .then((value) {
                          setState(() {
                            isLoading = false;
                          });
                          if (value == 200) {
                            Navigator.pushReplacementNamed(
                                context, Routes.addRole);
                          } else if (value == 400) {
                            ScaffoldMessenger.of(context).showSnackBar(
                              SnackBar(
                                  backgroundColor: Colors.red,
                                  content: AdaptiveText(
                                    text:
                                        'Пользователь с этой почтой уже зарегестрирован',
                                    maxLines: 2,
                                    fontWeight: FontWeight.w500,
                                    color: Colors.white,
                                  )),
                            );
                          } else {
                            ScaffoldMessenger.of(context).showSnackBar(
                              SnackBar(
                                  backgroundColor: Colors.red,
                                  content: AdaptiveText(
                                    text: 'Произошла ошибка',
                                    maxLines: 2,
                                    fontWeight: FontWeight.w500,
                                    color: Colors.white,
                                  )),
                            );
                          }
                        });
                      }
                    },
                  ),
                ),
              ),
            ),
            Center(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  AdaptiveText(
                    text: 'Есть аккаунт?',
                    fontSize: 12,
                    fontWeight: FontWeight.w400,
                  ),
                  TextButton(
                      onPressed: () {
                        Navigator.pushReplacementNamed(context, Routes.login);
                      },
                      child: AdaptiveText(
                        text: 'Войти',
                        fontWeight: FontWeight.w400,
                        color: AppColor.blue,
                        fontSize: 12,
                        textDecoration: TextDecoration.underline,
                      )),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class RegistrationScreen extends StatelessWidget {
  const RegistrationScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
        backgroundColor: AppColor.grey, body: RegistrationScreenBody());
  }
}
