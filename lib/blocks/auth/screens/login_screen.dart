import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:hackaton/blocks/auth/widgets/login_screen_body.dart';
import 'package:hackaton/static/repository/users.dart';

import '../../../static/app_color.dart';
import '../../../static/app_model.dart';
import '../../../static/sizehelpers.dart';
import '../../../ui/buttons/buttons.dart';
import '../../../ui/forms/input-form.dart';
import '../widgets/adaptive_text.dart';

import 'package:hackaton/routes.dart' as Routes;

class LoginScreen extends StatelessWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      backgroundColor: AppColor.grey,
      body: LoginScreenBody(),
    );
  }
}

class LoginForm extends StatefulWidget {
  const LoginForm({Key? key}) : super(key: key);

  @override
  State<LoginForm> createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginForm> {
  String userEmail = '';
  String userPassword = '';

  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    final mobile = isMobile(context);
    double offset = 100;
    if (mobile) {
      offset = 20;
    }
    return Container(
      margin: EdgeInsets.only(left: offset, right: offset),
      child: Form(
        key: _formKey,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const AutoSizeText(
                  "Электронная почта*",
                  textAlign: TextAlign.left,
                  style: TextStyle(
                      fontFamily: 'Golos',
                      fontSize: 12,
                      fontWeight: FontWeight.bold,
                      color: Colors.black),
                  maxLines: 1,
                ),
                const SizedBox(
                  height: 10,
                ),
                InputForm(
                    placeholder: 'ivanova@mail.ru',
                    onChange: (value) => setState(() {
                          userEmail = value;
                        })),
                const SizedBox(
                  height: 20,
                ),
                const AutoSizeText(
                  "Пароль*",
                  textAlign: TextAlign.left,
                  style: TextStyle(
                      fontFamily: 'Golos',
                      fontSize: 12,
                      fontWeight: FontWeight.bold,
                      color: Colors.black),
                  maxLines: 1,
                ),
                const SizedBox(
                  height: 10,
                ),
                InputForm(
                    placeholder: 'Введите пароль',
                    onChange: (value) => setState(() {
                          userPassword = value;
                        })),
                const SizedBox(
                  height: 50,
                ),
              ],
            ),
            Container(
              margin: const EdgeInsets.only(bottom: 12),
              child: Center(
                child: DefaultButton(
                  text: 'Войти',
                  onClick: () => {
                    if (_formKey.currentState!.validate())
                      {
                        loginUser(
                                user: User(
                                    id: null,
                                    email: userEmail,
                                    name: '',
                                    secondName: '',
                                    password: userPassword))
                            .then((value) {
                          if (value == 200) {
                            Navigator.pushNamed(context, Routes.profile);
                          } else if (value == 403) {
                            ScaffoldMessenger.of(context).showSnackBar(
                              SnackBar(
                                  backgroundColor: Colors.red,
                                  content: AdaptiveText(
                                    text: 'Неправильный пароль',
                                    maxLines: 2,
                                    fontWeight: FontWeight.w500,
                                    color: Colors.white,
                                  )),
                            );
                          } else {
                            ScaffoldMessenger.of(context).showSnackBar(
                              SnackBar(
                                  backgroundColor: Colors.red,
                                  content: AdaptiveText(
                                    text: 'Неправильный пароль',
                                    maxLines: 2,
                                    fontWeight: FontWeight.w500,
                                    color: Colors.white,
                                  )),
                            );
                          }
                        })
                      }
                  },
                ),
              ),
            ),
            Center(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  AdaptiveText(
                    text: 'Нет аккаунта?',
                    fontSize: 12,
                    fontWeight: FontWeight.w400,
                  ),
                  TextButton(
                      onPressed: () {
                        Navigator.pushReplacementNamed(
                            context, Routes.register);
                      },
                      child: AdaptiveText(
                        text: 'Зарегестрироваться',
                        fontWeight: FontWeight.w400,
                        color: AppColor.blue,
                        fontSize: 12,
                        textDecoration: TextDecoration.underline,
                      )),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
