import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

import '../../../ui/header.dart';
import '../data/events.dart';
import '../widgets/eventcard.dart';

class CatalogueScreen extends StatelessWidget {
  CatalogueScreen(){
    events.getEvents();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        margin: const EdgeInsets.only(bottom: 10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Header(),
            Expanded(
              child: Container(
                padding: const EdgeInsets.symmetric(vertical: 40, horizontal: 70),
                child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Text(
                    'Каталог мероприятий',
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 30,
                      fontWeight: FontWeight.w600
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.only(right: 500, bottom: 100),
                    child: const Text(
                    'Каталог вакансии от проверенных нами волонтерских организаций, которым нужна Ваша помощь. Иногда требуется разовая помощь, иногда постоянная. Вы можете выбрать то, что вам по душе и по силам.',
                    style: const TextStyle(
                      color: Color.fromARGB(113, 0, 0, 0),
                      fontSize: 12,
                      fontWeight: FontWeight.w400
                    ),
                  ),
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Expanded(
                        flex: 2,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Container(
                              padding: EdgeInsets.symmetric(vertical: 5),
                              decoration: const BoxDecoration(
                                color: Color(0xff333333),
                                borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(15),
                                  topRight: Radius.circular(15),
                                )
                              ),
                              child: const Text(
                                'Фильтры',
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 14,
                                  fontWeight: FontWeight.w500
                                ),
                              ),
                            ),
                            Container(
                              height: 400,
                              padding: EdgeInsets.symmetric(
                                vertical: 10,
                                horizontal: 15
                              ),
                              decoration: BoxDecoration(
                                color: Colors.white,
                              ),
                            )
                          ],
                        ),
                      ),
                      SizedBox(width: 30,),
                      Expanded(
                        flex: 6,
                        child: Observer(
                         builder: (_) =>  Container(
                           height: 650,
                           child: ListView(
                           scrollDirection: Axis.vertical,
                          children: [
                            for (var e in events.events) EventCard(event: e,)
                          ],
                        ),
                         ),)
                      )
                    ],
                  )
                ],
              ),
            ))
          ],
        ),
      ),
    );
  }
}
