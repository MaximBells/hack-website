import 'package:flutter/material.dart';
import 'package:hackaton/blocks/auth/widgets/pref_container.dart';
import 'package:hackaton/static/app_image.dart';
import 'package:hackaton/static/app_model.dart';
import 'package:hackaton/ui/buttons/buttons.dart';
import 'package:hackaton/ui/header.dart';

import '../../../static/app_enum.dart';
import '../../profile/widgets/anket_infio.dart';
import '../../profile/widgets/user_stats.dart';

class EventScreen extends StatelessWidget {
  Event event;
  EventScreen({required this.event});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
      Header(),
      Expanded(
          child: Container(
              padding: const EdgeInsets.symmetric(vertical: 40, horizontal: 70),
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      event.name,
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 30,
                          fontWeight: FontWeight.w600),
                    ),
                    Container(
                      margin: const EdgeInsets.only(right: 500),
                      child: Text(
                        event.description,
                        style: const TextStyle(
                            color: Color.fromARGB(113, 0, 0, 0),
                            fontSize: 12,
                            fontWeight: FontWeight.w400),
                      ),
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    statBox("Часы", event.hours.toString(),
                        Color.fromARGB(110, 157, 255, 60), AppImage.time),
                    SizedBox(
                      height: 15,
                    ),
                    statBox("Боброкоины", event.coins.toString(),
                        Color.fromARGB(102, 255, 212, 60), AppImage.bobrocoin),
                    SizedBox(
                      height: 15,
                    ),
                    anketData('Дата', event.dateTime, 15),
                    anketData('Место', 'г. Москва', 15),
                    Row(
                      children: event.prefs
                          .map((e) => PrefContainer(
                                text: prefsMap[e]!,
                                type: e,
                              ))
                          .toList(),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 40),
                      child: DefaultButton(
                          text: 'Откликнуться', onClick: () => {}),
                    )
                  ])))
    ]));
  }
}
