import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hackaton/blocks/catalogue/screens/event_screen.dart';
import 'package:hackaton/static/app_color.dart';
import 'package:hackaton/static/app_image.dart';
import 'package:hackaton/static/app_model.dart';
import 'package:hackaton/ui/buttons/buttons.dart';

import '../../profile/widgets/user_stats.dart';

class EventCard extends StatelessWidget{
  Event event;
  EventCard({required this.event});
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 20),
      padding: EdgeInsets.symmetric(
        vertical: 20,
        horizontal: 30
      ),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(15),
        color: Colors.white,
        boxShadow: [
        BoxShadow(
          color: Colors.black.withOpacity(0.2),
          spreadRadius: 1.5,
          blurRadius: 1,
          offset: Offset(1, 1), // changes position of shadow
        ),
    ],
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Expanded(
            flex: 2,
            child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            event.name,
            style: TextStyle(
              color: AppColor.blue,
              fontSize: 18,
              fontWeight: FontWeight.w600
            ),
          ),
          SizedBox(height: 15,),
          statBox("Часы", event.hours.toString(),
         Color.fromARGB(110, 157, 255, 60), AppImage.time),
         SizedBox(height: 15,),
          statBox("Боброкоины", event.coins.toString(),
              Color.fromARGB(102, 255, 212, 60), AppImage.bobrocoin),
        ],
      ),),
      Expanded(
        flex: 5,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          mainAxisSize: MainAxisSize.max,
          children: [
Text(
          event.description,
          style: TextStyle(
            color: Colors.black,
            fontWeight: FontWeight.w400,
            fontSize: 10
          ),
        ),
        Container(
          margin: EdgeInsets.only(
            top: 70
          ),
          child: DefaultButton(text: 'Откликнуться', onClick: ()=>{
            Navigator.push(context, new CupertinoPageRoute(builder: (_) => EventScreen(event: event)))
        }),
        )
          ],
        )
      )
        ],
      ),
    );
  }
}