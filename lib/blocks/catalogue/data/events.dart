import 'package:hackaton/static/app_enum.dart';
import 'package:hackaton/static/app_model.dart';
import 'package:mobx/mobx.dart';

import '../../../static/repository/events.dart';

part 'events.g.dart';

class Events = _Events with _$Events;

abstract class _Events with Store {

  @observable
  List<Event> events = [];

  @action
  Future<void> getEvents() async {
    final ev = await getAllEvents();
    events = ev;
  }
}

final events = Events();