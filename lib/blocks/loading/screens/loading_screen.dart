import 'package:animate_do/animate_do.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:hackaton/blocks/auth/screens/registration_screen.dart';
import 'package:hackaton/blocks/profile/screens/profile_screen.dart';
import 'package:hackaton/routes.dart' as Routes;
import 'package:hackaton/static/app.dart';
import 'package:mobx/mobx.dart';

class LoadingScreen extends StatelessWidget {
  const LoadingScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ReactionBuilder(
        child: Scaffold(
          body: Center(
            child: Pulse(
                duration: const Duration(milliseconds: 1000),
                delay: const Duration(seconds: 1),
                infinite: true,
                child: const FlutterLogo(
                  size: 200,
                )),
          ),
        ),
        builder: (context) => reaction((_) {
              App.initApp().then((value) {
                const storage = FlutterSecureStorage();
                storage.read(key: "USER_TOKEN").then((token) => {
                      Navigator.pushReplacement(
                          context,
                          CupertinoPageRoute(
                              builder: (context) => const RegistrationScreen(),
                              settings:
                                  const RouteSettings(name: Routes.register)))
                      // if (token == null)
                      //   {
                      //     Navigator.pushReplacement(
                      //         context,
                      //         CupertinoPageRoute(
                      //             builder: (context) =>
                      //                 const RegistrationScreen(),
                      //             settings: const RouteSettings(
                      //                 name: Routes.register)))
                      //   }
                      // else
                      //   {
                      //     Navigator.pushReplacement(
                      //         context,
                      //         CupertinoPageRoute(
                      //             builder: (context) => ProfileScreen(),
                      //             settings: const RouteSettings(
                      //                 name: Routes.profile)))
                      //   }
                    });
              });
            }, (result) {}));
  }
}
