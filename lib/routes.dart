const String profile = "/profile";
const String addRole = "/pickrole";
const String addVolunteerForm = "/volunteer/form";
const String addOrganisationForm = '/organisation/form';

const String register = "/register";
const String login = "/login";
const String catalog = '/catalog';