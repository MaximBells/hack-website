import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:hackaton/static/app_color.dart';

class InputForm extends StatelessWidget {
  final Function(String value) onChange;
  final String placeholder;
  TextEditingController? controller;
  VoidCallback? onPressed;
  TextInputType? textInputType;
  TextInputFormatter? textInputFormatter;
  int? minLines;
  int? maxLines;
  bool readOnly;
  bool mustWrite;
  Color backgroundColor;

  InputForm(
      {Key? key,
      required this.onChange,
      required this.placeholder,
      this.textInputType,
      this.controller,
      this.textInputFormatter,
      this.onPressed,
      this.maxLines,
      this.minLines,
      this.readOnly = false,
      this.mustWrite = true,
      this.backgroundColor = Colors.white})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: backgroundColor,
      constraints: BoxConstraints(
          maxWidth: minLines == null ? 505 : 1200, minWidth: 100),
      child: TextFormField(
        readOnly: readOnly,
        maxLines: maxLines,
        minLines: minLines,
        controller: controller,
        keyboardType: textInputType,
        maxLength: textInputFormatter == null ? null : 18,
        inputFormatters:
            textInputFormatter == null ? [] : [textInputFormatter!],
        onChanged: onChange,
        onTap: () {
          if (onPressed != null) {
            onPressed!();
          }
        },
        validator: (value) {
          if (mustWrite == true && (value == null || value.isEmpty)) {
            return 'Поле обязательно для заполнения';
          }
          return null;
        },
        decoration: InputDecoration(
            border: const OutlineInputBorder(
                borderRadius: BorderRadius.all(
              Radius.circular(4),
            )),
            filled: true,
            fillColor: Colors.white,
            hintText: placeholder,
            contentPadding: const EdgeInsets.all(20.0),
            hintStyle: const TextStyle(color: Color.fromARGB(53, 0, 0, 0))),
        style: const TextStyle(fontSize: 12),
      ),
    );
  }
}
