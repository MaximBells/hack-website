import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:hackaton/static/app_style.dart';

class DefaultButton extends StatelessWidget {
  String text = '';
  final VoidCallback onClick;

  DefaultButton({
    required this.text,
    required this.onClick
  });

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      style: AppStyle.standardStyle(context),
      onPressed: onClick, child: AutoSizeText(
      text,
      textAlign: TextAlign.center,
      style: TextStyle(
          fontFamily: 'Golos',
          fontSize: 12,
          fontWeight: FontWeight.normal,
          color: Colors.white),
      maxLines: 1,
    ),);
  }
}
/*
    return GestureDetector(
        onTap: this.onClick,
        child: Container(
            width: 300,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(10)),
              color: Color(0xff0044CC),
            ),
            child: Padding(
              padding: EdgeInsets.symmetric(
                  vertical: 15,
                  horizontal: 25
              ),
              child:,
            )

        )
    );

 */