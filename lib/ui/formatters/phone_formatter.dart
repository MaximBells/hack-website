import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class PhoneFormatter extends TextInputFormatter {
  @override
  TextEditingValue formatEditUpdate(
      TextEditingValue oldValue, TextEditingValue newValue) {
    String phone = newValue.text;

    if (phone.length == 1 && phone == "8" || phone == "7" || phone == "+") {
      phone = "+7";
    }

    if (phone.length == 1 && phone == "9") {
      phone = "+7 (9";
    }

    if (phone.length == 2) {
      phone += " (";
    }
    if (phone.length == 7) {
      phone += ") ";
    }

    if (phone.length == 12) {
      phone += "-";
    }
    if (phone.length == 15) {
      phone += "-";
    }

    if (oldValue.text.length > newValue.text.length) {
      return newValue;
    } else {
      return TextEditingValue(
          text: phone,
          selection: TextSelection.collapsed(offset: phone.length));
    }
  }
}

class EnterPhoneNumber extends StatefulWidget {
  final Function(String phone) didEdited;
  const EnterPhoneNumber(this.didEdited, {Key? key}) : super(key: key);

  @override
  _EnterPhoneNumberState createState() => _EnterPhoneNumberState();
}

class _EnterPhoneNumberState extends State<EnterPhoneNumber> {
  @override
  Widget build(BuildContext context) {
    return TextField(
        textAlign: TextAlign.center,
        decoration: const InputDecoration(
          hintText: "+7 (___) ___-__-__",
        ),
        keyboardType: TextInputType.phone,
        maxLength: 18,
        onChanged: (val) => {
          if (val.length == 18)
            {
              widget.didEdited(val
                  .replaceAll(" ", "")
                  .replaceAll("(", "")
                  .replaceAll(")", "")
                  .replaceAll("-", "")
                  .replaceAll("+", ""))
            }
        },
        inputFormatters: [PhoneFormatter()]);
  }
}
