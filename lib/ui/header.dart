import 'package:flutter/material.dart';
import 'package:hackaton/static/app_color.dart';
import 'package:hackaton/ui/buttons/buttons.dart';

import '../static/app_style.dart';

import 'package:hackaton/routes.dart' as Routes;

Widget headerLink(String title, VoidCallback onClick) {
  return GestureDetector(
    onTap: onClick,
    child: Container(
      margin: EdgeInsets.only(right: 70),
      child: Text(
        title,
        style: TextStyle(
            color: AppColor.blue, fontSize: 10, fontWeight: FontWeight.w400),
      ),
    ),
  );
}

class Header extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(color: AppColor.grey),
      padding: EdgeInsets.symmetric(vertical: 20, horizontal: 80),
      width: double.infinity,
      height: 100,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            'МосБобер',
            style: TextStyle(
                color: Colors.black, fontSize: 20, fontWeight: FontWeight.w600),
          ),
          Row(
            children: [
              headerLink('организовать', () => {}),
              headerLink('помочь',
                  () => {Navigator.pushNamed(context, Routes.catalog)}),
              headerLink('профиль',
                  () => {Navigator.pushNamed(context, Routes.profile)}),
            ],
          )
        ],
      ),
    );
  }
}
