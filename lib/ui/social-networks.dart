import 'package:flutter/material.dart';
import 'package:hackaton/static/sizehelpers.dart';

import '../static/app_image.dart';

Widget getImage(BuildContext context, String asset){
  bool mobile = isMobile(context);
  double size = mobile ? 40 : 65;
  return Container(
      height: size,
      width: size,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(1000)),
        border: Border.all(color: Color(0xffD7D7D7), width: 1)
      ),
      padding: EdgeInsets.all(mobile? 7 : 18),
      child: Image.asset(asset, fit: BoxFit.contain,)
  );
}

class SocialNetworks extends StatelessWidget{
  @override
  Widget build(BuildContext context) {

    return Row(
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        getImage(context, AppImage.vk),
        getImage(context, AppImage.telegram),
        getImage(context, AppImage.google),
        getImage(context, AppImage.apple),
      ],
    );
  }
}