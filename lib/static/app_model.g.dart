// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'app_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

User _$UserFromJson(Map<String, dynamic> json) => User(
      id: json['id'] as int?,
      email: json['email'] as String,
      name: json['name'] as String,
      secondName: json['secondName'] as String?,
      password: json['password'] as String,
      type: $enumDecodeNullable(_$UserTypeEnumMap, json['type']) ??
          UserType.Common,
      coins: json['coins'] as int?,
      hours: json['hours'] as int?,
    )
      ..organisation = json['organisation'] as int?
      ..volunteerform = json['volunteerform'] as int?
      ..events = json['events'] as int?;

Map<String, dynamic> _$UserToJson(User instance) => <String, dynamic>{
      'id': instance.id,
      'email': instance.email,
      'name': instance.name,
      'secondName': instance.secondName,
      'password': instance.password,
      'type': _$UserTypeEnumMap[instance.type],
      'organisation': instance.organisation,
      'volunteerform': instance.volunteerform,
      'hours': instance.hours,
      'coins': instance.coins,
      'events': instance.events,
    };

const _$UserTypeEnumMap = {
  UserType.Common: 'Common',
  UserType.Volunteer: 'Volunteer',
  UserType.Organisation: 'Organisation',
};

Event _$EventFromJson(Map<String, dynamic> json) => Event(
      id: json['id'] as int,
      description: json['description'] as String,
      dateTime: json['dateTime'] as String,
      creator: json['creator'] as int,
      hours: json['hours'] as int,
      coins: json['coins'] as int,
      name: json['name'] as String,
      prefs: (json['prefs'] as List<dynamic>?)
              ?.map((e) => $enumDecode(_$PrefsTypeEnumMap, e))
              .toList() ??
          const [PrefsType.animal, PrefsType.corporate],
    )..place = json['place'] as String?;

Map<String, dynamic> _$EventToJson(Event instance) => <String, dynamic>{
      'id': instance.id,
      'description': instance.description,
      'dateTime': instance.dateTime,
      'creator': instance.creator,
      'hours': instance.hours,
      'coins': instance.coins,
      'place': instance.place,
      'name': instance.name,
      'prefs': instance.prefs.map((e) => _$PrefsTypeEnumMap[e]).toList(),
    };

const _$PrefsTypeEnumMap = {
  PrefsType.social: 'social',
  PrefsType.ecology: 'ecology',
  PrefsType.culture: 'culture',
  PrefsType.media: 'media',
  PrefsType.event: 'event',
  PrefsType.sport: 'sport',
  PrefsType.patriotic: 'patriotic',
  PrefsType.animal: 'animal',
  PrefsType.corporate: 'corporate',
  PrefsType.medicine: 'medicine',
  PrefsType.sociality: 'sociality',
};

VolunteerForm _$VolunteerFormFromJson(Map<String, dynamic> json) =>
    VolunteerForm(
      id: json['id'] as int,
      description: json['description'] as String,
      phone: json['phone'] as String,
      prefs: (json['prefs'] as List<dynamic>)
          .map((e) => $enumDecode(_$PrefsTypeEnumMap, e))
          .toList(),
      hours: json['hours'] as int,
      coins: json['coins'] as int,
      userId: json['userId'] as int?,
    );

Map<String, dynamic> _$VolunteerFormToJson(VolunteerForm instance) =>
    <String, dynamic>{
      'id': instance.id,
      'description': instance.description,
      'phone': instance.phone,
      'prefs': instance.prefs.map((e) => _$PrefsTypeEnumMap[e]).toList(),
      'hours': instance.hours,
      'coins': instance.coins,
      'userId': instance.userId,
    };

Organisation _$OrganisationFromJson(Map<String, dynamic> json) => Organisation(
      id: json['id'] as int,
      nameOrganisation: json['nameOrganisation'] as String,
      emailOrganisation: json['emailOrganisation'] as String,
      description: json['description'] as String,
      site: json['site'] as String,
    );

Map<String, dynamic> _$OrganisationToJson(Organisation instance) =>
    <String, dynamic>{
      'id': instance.id,
      'nameOrganisation': instance.nameOrganisation,
      'emailOrganisation': instance.emailOrganisation,
      'description': instance.description,
      'site': instance.site,
    };

Submission _$SubmissionFromJson(Map<String, dynamic> json) => Submission(
      id: json['id'] as int,
      event: Event.fromJson(json['event'] as Map<String, dynamic>),
      userFrom: User.fromJson(json['userFrom'] as Map<String, dynamic>),
      userTo: User.fromJson(json['userTo'] as Map<String, dynamic>),
      letter: json['letter'] as String,
      type: $enumDecode(_$SubmissionTypesEnumMap, json['type']),
      status: $enumDecode(_$SubmissionStatusesEnumMap, json['status']),
    );

Map<String, dynamic> _$SubmissionToJson(Submission instance) =>
    <String, dynamic>{
      'id': instance.id,
      'event': instance.event,
      'userFrom': instance.userFrom,
      'userTo': instance.userTo,
      'letter': instance.letter,
      'type': _$SubmissionTypesEnumMap[instance.type],
      'status': _$SubmissionStatusesEnumMap[instance.status],
    };

const _$SubmissionTypesEnumMap = {
  SubmissionTypes.reply: 'reply',
  SubmissionTypes.apply: 'apply',
};

const _$SubmissionStatusesEnumMap = {
  SubmissionStatuses.waiting: 'waiting',
  SubmissionStatuses.accepted: 'accepted',
  SubmissionStatuses.declined: 'declined',
  SubmissionStatuses.confirmed: 'confirmed',
};

Transaction _$TransactionFromJson(Map<String, dynamic> json) => Transaction(
      id: json['id'] as int,
      coins: json['coins'] as int,
      hours: json['hours'] as int,
      description: json['description'] as String,
      from: User.fromJson(json['from'] as Map<String, dynamic>),
      to: User.fromJson(json['to'] as Map<String, dynamic>),
      event: Event.fromJson(json['event'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$TransactionToJson(Transaction instance) =>
    <String, dynamic>{
      'id': instance.id,
      'coins': instance.coins,
      'hours': instance.hours,
      'description': instance.description,
      'from': instance.from,
      'to': instance.to,
      'event': instance.event,
    };
