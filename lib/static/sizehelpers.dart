import 'package:flutter/material.dart';

const int widthBreakPoint = 800;

double getWidth(BuildContext context) {
  return MediaQuery.of(context).size.width;
}

double getHeight(BuildContext context) {
  return MediaQuery.of(context).size.height;
}

bool isMobile(BuildContext context) {
  return getWidth(context) < widthBreakPoint;
}