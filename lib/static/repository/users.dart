import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:hackaton/blocks/auth/screens/organisation_form.dart';
import 'package:hackaton/static/app.dart';
import 'package:hackaton/static/app_enum.dart';
import 'package:hackaton/static/app_model.dart';
import 'package:hackaton/static/repository/default.dart';

import 'consts.dart';

Future<int?> registerUser({required User user}) async {
  final api = getAnonymousApiClient();
  Map<String, dynamic> userJson = user.toJson();
  userJson.remove('organisation');
  userJson.remove('volunteerform');
  userJson.remove('id');
  userJson.remove('hours');
  userJson.remove('events');
  userJson.remove('coins');

  try {
    print(userJson);
    var res = await api.post('/users/registration', data: userJson);
    Map<String, dynamic> json = res.data;
    App.controller.initUser(
        newUser: User(
            id: json['id'],
            email: json['email'],
            name: json['name'],
            secondName: json['secondName'],
            type: userMap[json['type']] ?? UserType.Common,
            password: ''));
    print(App.controller.user!.toJson());
    App.controller.setAccessToken(token: json['accessToken']);
    const storage = FlutterSecureStorage();
    String token = res.data['accessToken'];
    await storage.write(key: "USER_TOKEN", value: token);
    await storage.write(key: "USER_ID", value: json['id'].toString());
    return res.statusCode;
  } on DioError catch (e) {
    print(e);
    return e.response?.statusCode;
  } catch (e) {
    print(e);
    return 505;
  }
  return 200;
}

Future<int?> loginUser({required User user}) async {
  final api = getAnonymousApiClient();
  Map<String, dynamic> userJson = user.toJson();

  userJson.remove('organisation');
  userJson.remove('volunteerform');
  userJson.remove('secondName');
  userJson.remove('name');
  userJson.remove('type');
  userJson.remove('id');
  userJson.remove('hours');
  userJson.remove('coins');
  userJson.remove('events');

  try {
    print(userJson);
    var res = await api.post('/users/login', data: userJson);
    Map<String, dynamic> json = res.data;
    App.controller.initUser(
        newUser: User(
            id: json['id'],
            email: json['email'],
            name: json['name'],
            secondName: json['secondName'],
            type: userMap[json['type']] ?? UserType.Common,
            password: ''));
    print(App.controller.user!.toJson());
    App.controller.setAccessToken(token: json['accessToken']);
    const storage = FlutterSecureStorage();
    String token = res.data['accessToken'];
    await storage.write(key: "USER_TOKEN", value: token);
  } on DioError catch (e) {
    print(e);
    return e.response?.statusCode;
  } catch (e) {
    print(e);
    return 505;
  }
  return 200;
}

Future<bool> updateType({required User user, required UserType type}) async {
  final api = getAnonymousApiClient();
  Map<String, dynamic> json = {'type': type.name};
  try {
    var res = await api.post('/users/updateType/${user.id!}', data: json);
    user.type = type;
    App.controller.initUser(newUser: user);
  } on DioError catch (e) {
    print(e);
    return false;
  } catch (e) {
    print(e);
    return false;
  }
  return true;
}

Future<bool> createVolunteer({required VolunteerForm form}) async {
  final api = getAnonymousApiClient();
  List<int> newPrefs = [];
  if (form.prefs.isNotEmpty) {
    for (var element in form.prefs) {
      newPrefs.add(prefsInt[element] ?? 1);
    }
  }
  var json = form.toJson();
  json.remove('prefs');
  json.remove('id');
  json.remove('userId');
  json.addAll({'prefs': newPrefs, 'userID': App.controller.user!.id});
  print(json);
  try {
    var res = await api.post('/volunteer/insert', data: json);
  } on DioError catch (e) {
    print(e);
    return false;
  } catch (e) {
    print(e);
    return false;
  }
  return true;
}

Future<bool> createOrganisation({required Organisation form}) async {
  final api = getAnonymousApiClient();
  var json = form.toJson();
  json.remove('id');
  json.remove('emailOrganisation');
  json.remove('nameOrganisation');
  json.addAll({
    'userID': App.controller.user!.id!,
    'email': form.emailOrganisation,
    'name': form.nameOrganisation,
  });
  try {
    print(json);
    var res = await api.post('/organization/insert', data: json);
    print(res);
  } on DioError catch (e) {
    print(e);
    return false;
  } catch (e) {
    print(e);
    return false;
  }
  return true;
}

Future<VolunteerForm> getProfile() async {
    const storage = FlutterSecureStorage();
    dynamic? i = await storage.read(key: "USER_ID");

    Dio client = getAnonymousApiClient();

    try {
      return client.get('volunteer/108').then((value) => VolunteerForm.fromJson(value.data));
    }  catch (e) {
      return VolunteerForm(coins: 1, description: 'ewdewdwekfn erwjnfernfier ver fver fveirv ',
                     phone: '+79897949179', hours: 3, id: 3, prefs: [PrefsType.animal, PrefsType.corporate]
                     );
    }
}