abstract class IResponse {
  String? error;
  bool? is_ok;

  IResponse({this.is_ok, this.error});
}