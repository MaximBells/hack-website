import 'package:dio/dio.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

// String baseUrl = 'https://c1a1-145-255-1-165.eu.ngrok.io/';
String baseUrl = 'https://cfeb-136-169-224-153.eu.ngrok.io';

Future<Dio> getApiClient() async {
  var _dio = Dio();
  _dio.options.baseUrl = baseUrl;

  final storage = new FlutterSecureStorage();

  var token = await storage.read(key: "USER_TOKEN");

  _dio.interceptors.clear();
  _dio.options.headers["Authorization"] = "Token " + token!;
  return _dio;
}

Dio getAnonymousApiClient() {
  var _dio = Dio();
  _dio.options.baseUrl = baseUrl;
  return _dio;
}
