import 'package:hackaton/static/app_model.dart';
import 'package:hackaton/static/repository/default.dart';

Future<List<Event>> getAllEvents() async {
  final api = await getAnonymousApiClient();
  final dataJson =(await api.get('/events')).data;
  List<Event> events = [];
  dataJson.forEach((e) => events.add(Event.fromJson(e)));
  return events;
}