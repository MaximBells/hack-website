// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'app_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic, no_leading_underscores_for_local_identifiers

mixin _$AppController on _AppController, Store {
  late final _$userAtom = Atom(name: '_AppController.user', context: context);

  @override
  User? get user {
    _$userAtom.reportRead();
    return super.user;
  }

  @override
  set user(User? value) {
    _$userAtom.reportWrite(value, super.user, () {
      super.user = value;
    });
  }

  late final _$accessTokenAtom =
      Atom(name: '_AppController.accessToken', context: context);

  @override
  String? get accessToken {
    _$accessTokenAtom.reportRead();
    return super.accessToken;
  }

  @override
  set accessToken(String? value) {
    _$accessTokenAtom.reportWrite(value, super.accessToken, () {
      super.accessToken = value;
    });
  }

  late final _$_AppControllerActionController =
      ActionController(name: '_AppController', context: context);

  @override
  void initUser({required User newUser}) {
    final _$actionInfo = _$_AppControllerActionController.startAction(
        name: '_AppController.initUser');
    try {
      return super.initUser(newUser: newUser);
    } finally {
      _$_AppControllerActionController.endAction(_$actionInfo);
    }
  }

  @override
  void setAccessToken({required String token}) {
    final _$actionInfo = _$_AppControllerActionController.startAction(
        name: '_AppController.setAccessToken');
    try {
      return super.setAccessToken(token: token);
    } finally {
      _$_AppControllerActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
user: ${user},
accessToken: ${accessToken}
    ''';
  }
}
