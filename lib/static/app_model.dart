import 'package:hackaton/static/app_enum.dart';
import 'package:json_annotation/json_annotation.dart';

part 'app_model.g.dart';

abstract class Model {

  Map<String, dynamic> toJson();

}

@JsonSerializable()
class User extends Model {
  final int? id;
  final String email;
  String name;
  String? secondName;
  String password;
  UserType type;
  int? organisation;
  int? volunteerform;
  int? hours;
  int? coins;
  int? events;

  User(
      {required this.id,
      required this.email,
      required this.name,
      required this.secondName,
      required this.password,
      this.type = UserType.Common,
      this.coins,
      this.hours});

  Map<String,dynamic> toJson() => _$UserToJson(this);

  factory User.fromJson(Map<String,dynamic> data) => _$UserFromJson(data);
}

@JsonSerializable()
class Event extends Model {
  final int id;
  String description;
  String dateTime;
  int creator;
  int hours;
  int coins;
  String? place;
  String name;
  List<PrefsType> prefs;

  Event(
      {required this.id,
      required this.description,
      required this.dateTime,
      required this.creator,
      required this.hours,
      required this.coins,
      required this.name,
      this.prefs = const [PrefsType.animal, PrefsType.corporate]});
  
  Map<String,dynamic> toJson() => _$EventToJson(this);

  factory Event.fromJson(Map<String,dynamic> data) => _$EventFromJson(data);
}

@JsonSerializable()
class VolunteerForm extends Model {
  int id;
  String description;
  String phone;
  List<PrefsType> prefs;
  int hours;
  int coins;
  int? userId;

  VolunteerForm(
      {required this.id,
      required this.description,
      required this.phone,
      required this.prefs,
      required this.hours,
      required this.coins,
      this.userId});
  
  Map<String,dynamic> toJson() => _$VolunteerFormToJson(this);

  factory VolunteerForm.fromJson(Map<String,dynamic> data) => _$VolunteerFormFromJson(data);

}

@JsonSerializable()
class Organisation extends Model {
  int id;
  String nameOrganisation;
  String emailOrganisation;
  String description;
  String site;

  Organisation(
      {required this.id,
      required this.nameOrganisation,
      required this.emailOrganisation,
      required this.description,
      required this.site});
  Map<String,dynamic> toJson() => _$OrganisationToJson(this);

  factory Organisation.fromJson(Map<String,dynamic> data) => _$OrganisationFromJson(data);
}

@JsonSerializable()
class Submission extends Model{
  final int id;
  final Event event;
  final User userFrom;
  final User userTo;
  String letter;
  final SubmissionTypes type;
  SubmissionStatuses status;

  Submission(
      {required this.id,
      required this.event,
      required this.userFrom,
      required this.userTo,
      required this.letter,
      required this.type,
      required this.status});
  Map<String,dynamic> toJson() => _$SubmissionToJson(this);

  factory Submission.fromJson(Map<String,dynamic> data) => _$SubmissionFromJson(data);
}

@JsonSerializable()
class Transaction extends Model{
  final int id;
  final int coins;
  final int hours;
  final String description;
  final User from;
  final User to;
  final Event event;

  Transaction(
      {required this.id,
      required this.coins,
      required this.hours,
      required this.description,
      required this.from,
      required this.to,
      required this.event});
  Map<String,dynamic> toJson() => _$TransactionToJson(this);

  factory Transaction.fromJson(Map<String,dynamic> data) => _$TransactionFromJson(data);
}
