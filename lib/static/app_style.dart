import 'package:flutter/material.dart';

import 'app_color.dart';

class AppStyle {
  static ButtonStyle outLineStyle(BuildContext context,
          {Color? backgroundColor}) =>
      ElevatedButton.styleFrom(
        padding:
            const EdgeInsets.only(left: 30, right: 30, top: 21, bottom: 21),
        shape: RoundedRectangleBorder(
            side: BorderSide(color: Theme.of(context).colorScheme.secondary),
            borderRadius: BorderRadius.circular(12)),
        primary: backgroundColor ?? AppColor.grey,
      );

  static ButtonStyle standardStyle(BuildContext context) =>
      ElevatedButton.styleFrom(
        padding:
            const EdgeInsets.only(left: 30, right: 30, top: 21, bottom: 21),
        shape: RoundedRectangleBorder(
            side: BorderSide(color: Theme.of(context).colorScheme.secondary),
            borderRadius: BorderRadius.circular(12)),
        primary: Theme.of(context).colorScheme.secondary,
      );
}
