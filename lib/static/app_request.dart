import 'package:dio/dio.dart';
import 'package:hackaton/static/repository/default.dart';

import 'app_model.dart';

Future<Event> getEvent (int id) async {
  Dio client = await getApiClient();
  return client.get('/event').then((res) => Event.fromJson(res.data));
}