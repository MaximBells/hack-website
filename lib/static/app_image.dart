class AppImage {
  static const logo = 'assets/logo.png';
  static const google = 'assets/google.png';
  static const telegram = 'assets/tg.png';
  static const apple = 'assets/apple.png';
  static const vk = 'assets/vk.png';
  static const volunteer = 'assets/volunteer.png';
  static const organisation = 'assets/organisation.png';
  static const time = 'assets/time.png';
  static const bobrocoin = 'assets/bobrocoin.png';
  static const hand = 'assets/hand.png';
}
