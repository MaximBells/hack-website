import 'package:get_it/get_it.dart';
import 'package:hackaton/static/app_controller.dart';
import 'package:shared_preferences/shared_preferences.dart';

final getIt = GetIt.instance;

class App {
  static Future<void> initApp() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if (!getIt.isRegistered<SharedPreferences>()) {
      getIt.registerLazySingleton<SharedPreferences>(() => prefs);
    }
    if (!getIt.isRegistered<AppController>()) {
      getIt.registerLazySingleton<AppController>(() => AppController());
    }
  }

  static SharedPreferences get prefs => getIt<SharedPreferences>();

  static AppController get controller => getIt<AppController>();
}


