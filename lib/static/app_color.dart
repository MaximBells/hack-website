import 'package:flutter/material.dart';

class AppColor {
  static const brown = Color.fromRGBO(223, 123, 5, 1);
  static const blue = Color.fromRGBO(0, 68, 204, 1);
  static const grey = Color.fromRGBO(240, 240, 240, 1);
  static const lightGrey = Color.fromRGBO(196, 200, 208, 1);
  static const lightBlue = Color.fromRGBO(221, 231, 255, 1);
}
