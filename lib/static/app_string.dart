class AppString {
  //auth/screens/login_screen
  static const youGetCaught = 'ВЫ ПОПАЛИ В НОРУ БОБРА';
  static const inviteBobrite = 'ПРИГЛАШАЕМ БОБРИТЬ БОБРО \nВМЕСТЕ';

  //auth/screens/role_registration
  static const registration = 'Регистрация';
  static const skip = 'Пропустить';
  static const next = 'Продолжить';

  //auth/widgets/choose_role
  static const iAmVolunteer = 'Я волонтер';
  static const iDoNotHaveOrganisation =
      'У меня нет организации. \nЯ хочу заниматься \nволонтерством \nили мне нужна помощь.';
  static const organisation = 'Организация';
  static const iRepresentOrganisation =
      'Я представляю \nНКО или фонд, занимаюсь организацией мероприятий.';
}
