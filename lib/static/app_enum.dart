enum UserType { Common, Volunteer, Organisation }

enum SubmissionStatuses { waiting, accepted, declined, confirmed }

enum SubmissionTypes { reply, apply }

enum PrefsType {
  social,
  ecology,
  culture,
  media,
  event,
  sport,
  patriotic,
  animal,
  corporate,
  medicine,
  sociality
}

const Map<PrefsType, String> prefsMap = {
  PrefsType.social: 'Социальное',
  PrefsType.ecology: 'Экологическое',
  PrefsType.culture: 'Культурное',
  PrefsType.media: 'Медиаволонтерство',
  PrefsType.event: 'Событийное',
  PrefsType.sport: 'Спортивное',
  PrefsType.patriotic: 'Патриотическое',
  PrefsType.animal: 'Зооволонтерство',
  PrefsType.corporate: 'Корпоративное',
  PrefsType.medicine: 'Медицинское',
  PrefsType.sociality: 'Общественная безопасность',
};
const Map<PrefsType, int> prefsInt = {
  PrefsType.social: 1,
  PrefsType.ecology: 2,
  PrefsType.culture: 3,
  PrefsType.media: 4,
  PrefsType.event: 5,
  PrefsType.sport: 6,
  PrefsType.patriotic: 7,
  PrefsType.animal: 8,
  PrefsType.corporate: 9,
  PrefsType.medicine: 10,
  PrefsType.sociality: 11,
};

const Map<String, UserType> userMap = {
  'Common': UserType.Common,
  'Volunteer': UserType.Volunteer,
  'Organisation': UserType.Organisation
};
