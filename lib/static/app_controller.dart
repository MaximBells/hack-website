import 'package:hackaton/static/app_model.dart';
import 'package:mobx/mobx.dart';

part 'app_controller.g.dart';

class AppController extends _AppController with _$AppController {}

abstract class _AppController with Store {
  @observable
  User? user;

  @observable
  String? accessToken;

  @action
  void initUser({required User newUser}) => user = newUser;

  @action
  void setAccessToken({required String token}) => accessToken = token;
}
