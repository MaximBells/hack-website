extension DateTimeExtension on DateTime {
  String get fixedDate {
    String result = '';
    DateTime date = this;
    result = '${date.dayFixed}.${date.monthFixed}.${date.year.toString()}';
    return result;
  }

  String get dayFixed {
    String result = '';
    var date = this;
    if (date.day >= 10) {
      result = date.day.toString();
    } else {
      result = '0${date.day}';
    }
    return result;
  }

  String get monthFixed {
    String result = '';
    var date = this;
    if (date.month >= 10) {
      result = date.month.toString();
    } else {
      result = '0${date.month}';
    }
    return result;
  }
}
